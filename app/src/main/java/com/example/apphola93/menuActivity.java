package com.example.apphola93;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class menuActivity extends AppCompatActivity {
    private CardView crvHola, crvImc, crvGrados, crvMonera, crvcotizacion, crvSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        iniciarComponentes();
        crvHola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        crvImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, imcActivity.class);
                startActivity(intent);
            }
        });

        crvGrados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, gradoActivity.class);
                startActivity(intent);
            }
        });

        crvMonera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, cambioActivity.class);
                startActivity(intent);
            }
        });

        crvcotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, ingresaCotizacionActivity.class);
                startActivity(intent);
            }
        });

        crvSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, spinnerperzonalizado.class);
                startActivity(intent);
            }
        });

        EdgeToEdge.enable(this);
        ViewGroup rootView = findViewById(android.R.id.content);
        ViewCompat.setOnApplyWindowInsetsListener(rootView, (v, insets) -> {
            v.setPadding(
                    insets.getSystemWindowInsetLeft(),
                    insets.getSystemWindowInsetTop(),
                    insets.getSystemWindowInsetRight(),
                    insets.getSystemWindowInsetBottom()
            );
            return insets.consumeSystemWindowInsets();
        });
    }

    private void iniciarComponentes() {
        crvHola = findViewById(R.id.crvHola);
        crvImc = findViewById(R.id.crvImc);
        crvMonera = findViewById(R.id.crvMoneda);
        crvGrados = findViewById(R.id.crvConversion);
        crvcotizacion = findViewById(R.id.crvCotizacion);
        crvSpinner = findViewById(R.id.crvSpiner);
    }
}

